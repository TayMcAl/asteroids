/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 69F044FD
/// @DnDApplyTo : 1f09ce31-c2d4-41b2-9244-c6dcf3710b86
with(object_pc) speed = 0;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4771E688
/// @DnDArgument : "var" "global.shield"
/// @DnDArgument : "value" "1"
if(global.shield == 1)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 34F7A8B3
	/// @DnDParent : 4771E688
	/// @DnDArgument : "spriteind" "sprite_ship"
	/// @DnDSaveInfo : "spriteind" "0d38418e-d0f0-455b-9707-d5ea2fe93dc1"
	sprite_index = sprite_ship;
	image_index = 0;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 58B864D8
	/// @DnDParent : 4771E688
	/// @DnDArgument : "steps" "10"
	/// @DnDArgument : "alarm" "2"
	alarm_set(2, 10);

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 1B0C0609
	/// @DnDParent : 4771E688
	/// @DnDArgument : "steps" "280"
	alarm_set(0, 280);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 641C1577
/// @DnDArgument : "var" "global.shield"
if(global.shield == 0)
{
	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 6AF0677C
	/// @DnDParent : 641C1577
	/// @DnDArgument : "steps" "1"
	/// @DnDArgument : "alarm" "1"
	alarm_set(1, 1);
}