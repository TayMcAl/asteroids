/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1C1A7448
/// @DnDArgument : "var" "global.shield"
/// @DnDArgument : "value" "1"
if(global.shield == 1)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 2396AF8A
	/// @DnDParent : 1C1A7448
	/// @DnDArgument : "spriteind" "sprite_ship"
	/// @DnDSaveInfo : "spriteind" "0d38418e-d0f0-455b-9707-d5ea2fe93dc1"
	sprite_index = sprite_ship;
	image_index = 0;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 1E2A9C64
	/// @DnDParent : 1C1A7448
	/// @DnDArgument : "steps" "10"
	/// @DnDArgument : "alarm" "2"
	alarm_set(2, 10);

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 07879B14
	/// @DnDParent : 1C1A7448
	/// @DnDArgument : "steps" "280"
	alarm_set(0, 280);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 408FF317
/// @DnDArgument : "var" "global.shield"
if(global.shield == 0)
{
	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 0BEF432E
	/// @DnDParent : 408FF317
	/// @DnDArgument : "steps" "1"
	/// @DnDArgument : "alarm" "1"
	alarm_set(1, 1);
}