/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 061BA7A9
/// @DnDArgument : "var" "tutorial"
global.tutorial = 0;

/// @DnDAction : YoYo Games.Rooms.If_First_Room
/// @DnDVersion : 1
/// @DnDHash : 3680548E
if(room == room_first)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 1F38D962
	/// @DnDParent : 3680548E
	/// @DnDArgument : "value" "1"
	/// @DnDArgument : "var" "tutorial"
	global.tutorial = 1;
}

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 479BC139
/// @DnDApplyTo : 1f09ce31-c2d4-41b2-9244-c6dcf3710b86
/// @DnDArgument : "var" "enemy"
with(object_pc) {
global.enemy = 0;

}

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 0C4DC2B5
/// @DnDApplyTo : 1f09ce31-c2d4-41b2-9244-c6dcf3710b86
/// @DnDArgument : "value" "1"
/// @DnDArgument : "var" "shield"
with(object_pc) {
global.shield = 1;

}

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 2C68B276
/// @DnDArgument : "steps" "100"
/// @DnDArgument : "alarm" "3"
alarm_set(3, 100);