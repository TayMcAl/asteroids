{
    "id": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_pc",
    "eventList": [
        {
            "id": "b1a4b43e-97d6-4e4e-8df1-cc6e39382ae6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "97d0b6d8-bcf2-494d-ab9b-3345197028ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "0c2a8880-ab12-48b7-b989-5e8774363b39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "985e5910-a9ae-4612-8221-3cfd5df3a598",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "4865c0bf-c6c1-484f-bd04-35058b270699",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "14a70ddb-517b-46a4-b0e0-9082692290c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "8f085513-9e2f-4c47-858a-caaffb3489f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "758f37bf-6148-4d34-9ec2-646da5c76978",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 9,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "efeef7da-5769-4d84-aa27-b909820b7c9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "b173bbac-a605-4008-8d4e-72a4632a75ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6c17881a-7d0b-4be2-93cb-1e6d8237f163",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "cbbfed7b-3987-4295-896f-e09b9a864179",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "e3f8a1e9-a630-4f3b-bc37-aa4c2769aa98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "4cb2a92f-9958-44e1-89e0-ed1a6fe9f836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "2873fabd-c8d5-4b00-b5fe-1761d201f7da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "8538bed4-af39-4cc8-90c1-5943d6403b19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "0b8abab4-e1a4-464a-93df-786b73c4d562",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "85b1c770-11f6-4ab0-b4f1-3106f43d91c7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "d1a8ea50-78f2-44b6-84cd-5b692d19ea33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "ec37df74-dac4-4ff4-b951-81204e925f46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "2f052b44-de36-4e16-bff3-fa467e601491",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1625c455-3ec8-41b4-b40e-bcd034ba6f65",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "e890eb02-65a2-40c1-bbc2-e74272ceab43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "719618f7-da91-4f08-b814-c6709104dc2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "f2cd2db3-4f3c-4c3b-a4eb-985d56a90b03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "69972156-572e-4547-a014-005f3aaf9f35",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "ec3699c3-e455-453e-bf64-31e6248b8802",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "c06853e7-93ee-4047-8020-b134ef438b3b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "6fe3b93a-cc1e-4cc6-9093-be074e32e46c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "48a646ea-c25e-4836-97ff-1345cf6c6c4f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        },
        {
            "id": "1035c0c8-2e64-43cf-8f68-3924c4a76742",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "33e580eb-884c-4680-bc0c-7ef1896ac31e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f09ce31-c2d4-41b2-9244-c6dcf3710b86"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f3ca0dd-5397-4b46-8dd2-7130117e859e",
    "visible": true
}