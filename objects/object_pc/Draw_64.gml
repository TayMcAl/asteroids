/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 54EE598A
/// @DnDArgument : "var" "global.tutorial"
/// @DnDArgument : "value" "1"
if(global.tutorial == 1)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 72F9A176
	/// @DnDParent : 54EE598A
	/// @DnDArgument : "font" "font_tutorial"
	/// @DnDSaveInfo : "font" "3ac3201a-4546-422a-9016-f6b07fc9b53a"
	draw_set_font(font_tutorial);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 1038311D
	/// @DnDApplyTo : 1f09ce31-c2d4-41b2-9244-c6dcf3710b86
	/// @DnDParent : 54EE598A
	/// @DnDArgument : "x" "-260"
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y" "50"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "caption" ""Tutorial: ""
	/// @DnDArgument : "var" ""Arrow keys to move, press SHIFT to dash, CTRL to brake, SPACE to shoot""
	with(object_pc) draw_text(x + -260, y + 50, string("Tutorial: ") + string("Arrow keys to move, press SHIFT to dash, CTRL to brake, SPACE to shoot"));
}