{
    "id": "1625c455-3ec8-41b4-b40e-bcd034ba6f65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_spike",
    "eventList": [
        {
            "id": "d29a0b9f-e366-4a05-b2c6-bb61d75e7d19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1625c455-3ec8-41b4-b40e-bcd034ba6f65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "047cb358-1403-4722-99a8-2bfa18ea7149",
    "visible": true
}