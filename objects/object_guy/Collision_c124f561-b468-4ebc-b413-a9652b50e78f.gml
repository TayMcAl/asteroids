/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 5C153FE7
/// @DnDArgument : "obj" "object_father"
/// @DnDSaveInfo : "obj" "69972156-572e-4547-a014-005f3aaf9f35"
var l5C153FE7_0 = false;
l5C153FE7_0 = instance_exists(object_father);
if(l5C153FE7_0)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 34B76F60
	/// @DnDParent : 5C153FE7
	/// @DnDArgument : "var" "global.intro"
	/// @DnDArgument : "value" "1"
	if(global.intro == 1)
	{
		/// @DnDAction : YoYo Games.Common.Set_Global
		/// @DnDVersion : 1
		/// @DnDHash : 4E918FAC
		/// @DnDParent : 34B76F60
		/// @DnDArgument : "value" "1"
		/// @DnDArgument : "var" "warn"
		global.warn = 1;
	}

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 7C48F4BC
	/// @DnDParent : 5C153FE7
	/// @DnDArgument : "alarm" "1"
	alarm_set(1, 30);
}

/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 74CB77F6
/// @DnDArgument : "obj" "object_father"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "69972156-572e-4547-a014-005f3aaf9f35"
var l74CB77F6_0 = false;
l74CB77F6_0 = instance_exists(object_father);
if(!l74CB77F6_0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 34C721F9
	/// @DnDApplyTo : 4551a2e4-576e-4519-9d3b-6f2ff0ff3435
	/// @DnDParent : 74CB77F6
	with(object_bullet) instance_destroy();

	/// @DnDAction : YoYo Games.Instance Variables.Set_Health
	/// @DnDVersion : 1
	/// @DnDHash : 06C66A1E
	/// @DnDParent : 74CB77F6
	/// @DnDArgument : "health" "-1"
	/// @DnDArgument : "health_relative" "1"
	if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
	__dnd_health += real(-1);
}