/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 68DF6229
/// @DnDArgument : "font" "font_guy"
/// @DnDSaveInfo : "font" "b2b1a155-9fa5-43d3-85d9-0cb9b91b6bad"
draw_set_font(font_guy);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 673FC552
/// @DnDArgument : "var" "global.intro"
/// @DnDArgument : "value" "1"
if(global.intro == 1)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 33A6EE50
	/// @DnDParent : 673FC552
	draw_set_colour($FFFFFFFF & $ffffff);
	draw_set_alpha(($FFFFFFFF >> 24) / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 1C0732FF
	/// @DnDParent : 673FC552
	/// @DnDArgument : "caption" ""Guy Fieri: ""
	/// @DnDArgument : "var" ""Enraged Titan of Suffering Incarnate""
	draw_text(0, 0, string("Guy Fieri: ") + string("Enraged Titan of Suffering Incarnate"));
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 71DB0567
/// @DnDArgument : "var" "global.boss"
/// @DnDArgument : "value" "1"
if(global.boss == 1)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 06C558B0
	/// @DnDParent : 71DB0567
	/// @DnDArgument : "color" "$FF0000FF"
	draw_set_colour($FF0000FF & $ffffff);
	draw_set_alpha(($FF0000FF >> 24) / $ff);

	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 2FED8C5C
	/// @DnDParent : 71DB0567
	/// @DnDArgument : "font" "font_guy"
	/// @DnDSaveInfo : "font" "b2b1a155-9fa5-43d3-85d9-0cb9b91b6bad"
	draw_set_font(font_guy);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 063DB8B0
	/// @DnDParent : 71DB0567
	/// @DnDArgument : "x" "200"
	/// @DnDArgument : "y" "450"
	/// @DnDArgument : "caption" ""Fool! ""
	/// @DnDArgument : "var" ""You thought I would let you escape Flavortown that easily?!?!""
	draw_text(200, 450, string("Fool! ") + string("You thought I would let you escape Flavortown that easily?!?!"));
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4FF54C23
/// @DnDArgument : "var" "global.warn"
/// @DnDArgument : "value" "1"
if(global.warn == 1)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 4EB0ACF3
	/// @DnDParent : 4FF54C23
	draw_set_colour($FFFFFFFF & $ffffff);
	draw_set_alpha(($FFFFFFFF >> 24) / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 5DC3357E
	/// @DnDParent : 4FF54C23
	/// @DnDArgument : "x" "250"
	/// @DnDArgument : "y" "450"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" ""Guy is too strong to damage while his thralls still live!""
	draw_text(250, 450, string("") + string("Guy is too strong to damage while his thralls still live!"));
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6A700811
/// @DnDArgument : "var" "global.damage"
/// @DnDArgument : "value" "1"
if(global.damage == 1)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 6D5336F0
	/// @DnDParent : 6A700811
	/// @DnDArgument : "color" "$FF0000FF"
	draw_set_colour($FF0000FF & $ffffff);
	draw_set_alpha(($FF0000FF >> 24) / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 7D2331E7
	/// @DnDParent : 6A700811
	/// @DnDArgument : "x" "450"
	/// @DnDArgument : "y" "450"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" ""Arrrgh...!""
	draw_text(450, 450, string("") + string("Arrrgh...!"));
}