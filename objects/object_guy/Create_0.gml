/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 07FEFFA5
/// @DnDArgument : "var" "damage"
global.damage = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 07AFA7E6
/// @DnDArgument : "var" "warn"
global.warn = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 32FB5B20
/// @DnDArgument : "var" "intro"
global.intro = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 725CFD19
/// @DnDArgument : "value" "1"
/// @DnDArgument : "var" "boss"
global.boss = 1;

/// @DnDAction : YoYo Games.Instance Variables.Set_Health
/// @DnDVersion : 1
/// @DnDHash : 2A986C70
/// @DnDArgument : "health" "20"

__dnd_health = real(20);

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 2D11120D
/// @DnDArgument : "steps" "100"
alarm_set(0, 100);

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 58D8E817
/// @DnDArgument : "steps" "250"
/// @DnDArgument : "alarm" "1"
alarm_set(1, 250);