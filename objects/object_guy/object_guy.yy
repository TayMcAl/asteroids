{
    "id": "c06853e7-93ee-4047-8020-b134ef438b3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_guy",
    "eventList": [
        {
            "id": "648aa362-e024-4453-8977-4b522d740a8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        },
        {
            "id": "b241aeb3-3568-491f-8242-cd077a2c50ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        },
        {
            "id": "5e6b2a3e-56bc-43c6-b5c7-fbd00eecf2c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        },
        {
            "id": "c124f561-b468-4ebc-b413-a9652b50e78f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4551a2e4-576e-4519-9d3b-6f2ff0ff3435",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        },
        {
            "id": "3dba0be7-7fe1-4746-87f9-0bd5c8d49fa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        },
        {
            "id": "5fb964f8-82c9-4abc-afba-8a1c1398f35e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        },
        {
            "id": "de30cd93-2dbb-44b6-aba5-0765271f8fb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "c06853e7-93ee-4047-8020-b134ef438b3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7314f476-eb1d-44cd-9abf-01c75f278530",
    "visible": true
}