/// @DnDAction : YoYo Games.Instance Variables.If_Health
/// @DnDVersion : 1
/// @DnDHash : 342C1206
/// @DnDArgument : "op" "3"
if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
if(__dnd_health <= 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 75A915FA
	/// @DnDParent : 342C1206
	instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 2D5CF90A
	/// @DnDApplyTo : 1625c455-3ec8-41b4-b40e-bcd034ba6f65
	/// @DnDParent : 342C1206
	with(object_spike) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 1E08D576
	/// @DnDApplyTo : 48a646ea-c25e-4836-97ff-1345cf6c6c4f
	/// @DnDParent : 342C1206
	with(object_barrier) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 44C08F85
	/// @DnDParent : 342C1206
	/// @DnDArgument : "xpos" "512"
	/// @DnDArgument : "ypos" "100"
	/// @DnDArgument : "objectid" "object_goal"
	/// @DnDSaveInfo : "objectid" "33e580eb-884c-4680-bc0c-7ef1896ac31e"
	instance_create_layer(512, 100, "Instances", object_goal);
}

/// @DnDAction : YoYo Games.Instance Variables.If_Health
/// @DnDVersion : 1
/// @DnDHash : 6BE4EC52
/// @DnDArgument : "value" "10"
if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
if(__dnd_health == 10)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 43288AB8
	/// @DnDParent : 6BE4EC52
	/// @DnDArgument : "value" "1"
	/// @DnDArgument : "var" "damage"
	global.damage = 1;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 21D8F782
	/// @DnDParent : 6BE4EC52
	/// @DnDArgument : "alarm" "2"
	alarm_set(2, 30);
}