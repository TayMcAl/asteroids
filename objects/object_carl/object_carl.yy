{
    "id": "85b1c770-11f6-4ab0-b4f1-3106f43d91c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_carl",
    "eventList": [
        {
            "id": "69a229c1-ff57-4361-bc6c-d143331ba125",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "85b1c770-11f6-4ab0-b4f1-3106f43d91c7"
        },
        {
            "id": "1decffb5-c7eb-429a-abd7-4cca3fad093c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "85b1c770-11f6-4ab0-b4f1-3106f43d91c7"
        },
        {
            "id": "2de989c2-2e1b-424b-80b0-31a07f32073f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4551a2e4-576e-4519-9d3b-6f2ff0ff3435",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "85b1c770-11f6-4ab0-b4f1-3106f43d91c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59ba3ee2-9c26-4bc6-9e7e-4a6771d8a9fc",
    "visible": true
}