/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 6744E406
/// @DnDArgument : "obj" "object_ast_sm"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "e0a1a62e-2b6d-4311-a945-a9a53b861739"
var l6744E406_0 = false;
l6744E406_0 = instance_exists(object_ast_sm);
if(!l6744E406_0)
{
	/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
	/// @DnDVersion : 1
	/// @DnDHash : 073F853C
	/// @DnDParent : 6744E406
	/// @DnDArgument : "obj" "object_ast"
	/// @DnDArgument : "not" "1"
	/// @DnDSaveInfo : "obj" "6c17881a-7d0b-4be2-93cb-1e6d8237f163"
	var l073F853C_0 = false;
	l073F853C_0 = instance_exists(object_ast);
	if(!l073F853C_0)
	{
		/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
		/// @DnDVersion : 1
		/// @DnDHash : 4810609B
		/// @DnDParent : 073F853C
		/// @DnDArgument : "obj" "object_carl"
		/// @DnDArgument : "not" "1"
		/// @DnDSaveInfo : "obj" "85b1c770-11f6-4ab0-b4f1-3106f43d91c7"
		var l4810609B_0 = false;
		l4810609B_0 = instance_exists(object_carl);
		if(!l4810609B_0)
		{
			/// @DnDAction : YoYo Games.Instances.Destroy_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 7A54A946
			/// @DnDApplyTo : all
			/// @DnDParent : 4810609B
			with(all) instance_destroy();
		
			/// @DnDAction : YoYo Games.Rooms.Next_Room
			/// @DnDVersion : 1
			/// @DnDHash : 4A980598
			/// @DnDParent : 4810609B
			room_goto_next();
		}
	}
}