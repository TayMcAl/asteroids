{
    "id": "69972156-572e-4547-a014-005f3aaf9f35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_father",
    "eventList": [
        {
            "id": "87fe52af-1f4a-4799-8c2c-dcaaaed3aab4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4551a2e4-576e-4519-9d3b-6f2ff0ff3435",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "69972156-572e-4547-a014-005f3aaf9f35"
        },
        {
            "id": "a8c54d4b-c18b-471c-bc9b-284a2014dd77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69972156-572e-4547-a014-005f3aaf9f35"
        },
        {
            "id": "0b6841fb-6f0c-43ca-992f-bf4c84f69b15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "69972156-572e-4547-a014-005f3aaf9f35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd845da7-0354-464e-82c8-5a300dad1430",
    "visible": true
}