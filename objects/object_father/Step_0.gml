/// @DnDAction : YoYo Games.Instance Variables.If_Health
/// @DnDVersion : 1
/// @DnDHash : 342C1206
/// @DnDArgument : "op" "3"
if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
if(__dnd_health <= 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 75A915FA
	/// @DnDParent : 342C1206
	instance_destroy();
}

/// @DnDAction : YoYo Games.Instance Variables.If_Health
/// @DnDVersion : 1
/// @DnDHash : 7A24E262
/// @DnDArgument : "op" "3"
/// @DnDArgument : "value" "7"
if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
if(__dnd_health <= 7)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 1DF5C2D8
	/// @DnDParent : 7A24E262
	/// @DnDArgument : "spriteind" "sprite_father_alt"
	/// @DnDSaveInfo : "spriteind" "8ad0754d-4f61-4d97-a263-9d85ad0d1a2c"
	sprite_index = sprite_father_alt;
	image_index = 0;
}