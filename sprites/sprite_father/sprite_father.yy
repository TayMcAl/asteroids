{
    "id": "bd845da7-0354-464e-82c8-5a300dad1430",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_father",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 426,
    "bbox_left": 90,
    "bbox_right": 408,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d60abe6-cf12-416e-9464-441623e33098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd845da7-0354-464e-82c8-5a300dad1430",
            "compositeImage": {
                "id": "ad5244ba-49fc-479c-a7c8-52931c2d2184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d60abe6-cf12-416e-9464-441623e33098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d3e1ab-5daf-4aa7-9cc5-0fe3361c4108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d60abe6-cf12-416e-9464-441623e33098",
                    "LayerId": "4c515eec-667f-45a2-bb24-98853c2f9fba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 433,
    "layers": [
        {
            "id": "4c515eec-667f-45a2-bb24-98853c2f9fba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd845da7-0354-464e-82c8-5a300dad1430",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 250,
    "yorig": 216
}