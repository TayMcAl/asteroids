{
    "id": "7aab70ed-0b77-4f9f-ae22-84bb866008e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_st_spike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0d0dc6f-b75d-4fde-a536-1c92ca326c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aab70ed-0b77-4f9f-ae22-84bb866008e0",
            "compositeImage": {
                "id": "0f102263-68c9-4743-89a3-70619c787d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d0dc6f-b75d-4fde-a536-1c92ca326c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3fef25f-e600-4dd3-8e30-f49775bb9468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d0dc6f-b75d-4fde-a536-1c92ca326c81",
                    "LayerId": "fe2d1419-c5fd-4244-9600-964205f632ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "fe2d1419-c5fd-4244-9600-964205f632ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7aab70ed-0b77-4f9f-ae22-84bb866008e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 32
}