{
    "id": "1fa48114-69b5-402f-bb35-ad192354e19e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bg_alt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd07fb39-cbf8-47a8-b8ab-e29b20127034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fa48114-69b5-402f-bb35-ad192354e19e",
            "compositeImage": {
                "id": "57f1d32e-8b76-4084-b235-8c5a57fdc38c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd07fb39-cbf8-47a8-b8ab-e29b20127034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98dcdd29-3410-4e5d-a066-cda02ec9a72f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd07fb39-cbf8-47a8-b8ab-e29b20127034",
                    "LayerId": "e4306f62-6b6f-4d7f-9e61-389a4840ea94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e4306f62-6b6f-4d7f-9e61-389a4840ea94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fa48114-69b5-402f-bb35-ad192354e19e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}