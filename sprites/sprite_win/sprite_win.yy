{
    "id": "b75ccc4e-9d9f-4acc-880c-9aa457d524cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_win",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 275,
    "bbox_left": 112,
    "bbox_right": 644,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f5b219f-86e2-44af-a98d-3b3ae55f4e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b75ccc4e-9d9f-4acc-880c-9aa457d524cd",
            "compositeImage": {
                "id": "6c8294ad-5d9c-4076-a551-f4de5a986285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5b219f-86e2-44af-a98d-3b3ae55f4e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be71599c-25f7-42c3-9018-0b730e969e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5b219f-86e2-44af-a98d-3b3ae55f4e5d",
                    "LayerId": "b76823c8-ebfa-4e4b-8cf8-41bade0853b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 422,
    "layers": [
        {
            "id": "b76823c8-ebfa-4e4b-8cf8-41bade0853b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b75ccc4e-9d9f-4acc-880c-9aa457d524cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 750,
    "xorig": 375,
    "yorig": 211
}