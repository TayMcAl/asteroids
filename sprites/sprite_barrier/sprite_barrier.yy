{
    "id": "bf6f28d2-3175-4fb1-be89-d50c184959c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_barrier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91928064-6f5a-4d5c-85fb-5da6d1b75df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf6f28d2-3175-4fb1-be89-d50c184959c2",
            "compositeImage": {
                "id": "6e0b3b8a-74e5-4f2b-8480-cc49b36d1c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91928064-6f5a-4d5c-85fb-5da6d1b75df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b140832b-607b-4a28-bc0a-4ed2e6ed5c2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91928064-6f5a-4d5c-85fb-5da6d1b75df7",
                    "LayerId": "8cf9a9b5-c3a9-41c6-a415-56dc02a59181"
                }
            ]
        },
        {
            "id": "007bf415-778d-4eec-b9e9-0fa61ea060c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf6f28d2-3175-4fb1-be89-d50c184959c2",
            "compositeImage": {
                "id": "23298682-268c-4c79-ac8f-ab617504b1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007bf415-778d-4eec-b9e9-0fa61ea060c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f7bd663-dbdd-4ad9-831b-360806940d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007bf415-778d-4eec-b9e9-0fa61ea060c6",
                    "LayerId": "8cf9a9b5-c3a9-41c6-a415-56dc02a59181"
                }
            ]
        },
        {
            "id": "7f9325d2-421b-4034-9841-07fa3717965b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf6f28d2-3175-4fb1-be89-d50c184959c2",
            "compositeImage": {
                "id": "524a78c8-eadb-4048-a801-9a5c02a67ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9325d2-421b-4034-9841-07fa3717965b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b9d88e4-589c-4b2a-b836-fa7d51f0e191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9325d2-421b-4034-9841-07fa3717965b",
                    "LayerId": "8cf9a9b5-c3a9-41c6-a415-56dc02a59181"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "8cf9a9b5-c3a9-41c6-a415-56dc02a59181",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf6f28d2-3175-4fb1-be89-d50c184959c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 32
}