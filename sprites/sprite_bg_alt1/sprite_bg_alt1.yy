{
    "id": "1e0539d4-b71c-4bec-b204-5a1de8a27d3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bg_alt1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f03735b2-8b8e-476c-816b-3faf08d24f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e0539d4-b71c-4bec-b204-5a1de8a27d3a",
            "compositeImage": {
                "id": "62f1f63a-2666-479f-869f-a5995e55d3b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03735b2-8b8e-476c-816b-3faf08d24f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49eb001c-24c8-4a2f-9745-89aab4c8af24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03735b2-8b8e-476c-816b-3faf08d24f67",
                    "LayerId": "0b4789c9-798e-43ea-9f08-b93ceecfcbf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0b4789c9-798e-43ea-9f08-b93ceecfcbf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e0539d4-b71c-4bec-b204-5a1de8a27d3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}