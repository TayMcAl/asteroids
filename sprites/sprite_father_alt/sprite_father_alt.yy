{
    "id": "8ad0754d-4f61-4d97-a263-9d85ad0d1a2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_father_alt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 409,
    "bbox_left": 126,
    "bbox_right": 414,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3363c09-de9e-4b73-a0f2-69b87d47f14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ad0754d-4f61-4d97-a263-9d85ad0d1a2c",
            "compositeImage": {
                "id": "fa2f18bd-7277-42dc-ad0a-7e9474f22aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3363c09-de9e-4b73-a0f2-69b87d47f14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93ba8479-00e5-451b-a570-2861c279efc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3363c09-de9e-4b73-a0f2-69b87d47f14e",
                    "LayerId": "d7f0b767-e67d-480f-a5a4-05e2f8533d42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 433,
    "layers": [
        {
            "id": "d7f0b767-e67d-480f-a5a4-05e2f8533d42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ad0754d-4f61-4d97-a263-9d85ad0d1a2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 250,
    "yorig": 216
}