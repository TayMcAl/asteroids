{
    "id": "7314f476-eb1d-44cd-9abf-01c75f278530",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_guy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 200,
    "bbox_left": 0,
    "bbox_right": 137,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8395a64-6bd7-4094-ac9d-78856d427bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7314f476-eb1d-44cd-9abf-01c75f278530",
            "compositeImage": {
                "id": "e8369720-fae0-4125-8aa6-56aea5f8ff09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8395a64-6bd7-4094-ac9d-78856d427bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a358c89-147e-4a83-93da-f27cbe0387ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8395a64-6bd7-4094-ac9d-78856d427bc8",
                    "LayerId": "81144ea6-e05b-4128-97ec-55766cd0c030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 213,
    "layers": [
        {
            "id": "81144ea6-e05b-4128-97ec-55766cd0c030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7314f476-eb1d-44cd-9abf-01c75f278530",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 138,
    "xorig": 69,
    "yorig": 106
}