{
    "id": "ee13c2ad-2b54-4abd-aa8e-d4668d453582",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_goal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 4,
    "bbox_right": 125,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "955a9146-3c89-4417-bca0-b2a39cd0a5aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee13c2ad-2b54-4abd-aa8e-d4668d453582",
            "compositeImage": {
                "id": "75ff28be-8cdb-4918-99d4-0f4ef501ca28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955a9146-3c89-4417-bca0-b2a39cd0a5aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f122be-dd47-4e7b-9c15-d6348db3b29e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955a9146-3c89-4417-bca0-b2a39cd0a5aa",
                    "LayerId": "c72f2e7d-4388-4d66-abfc-64683c18e835"
                }
            ]
        },
        {
            "id": "6a69e16d-3a76-427b-98c8-fcf9e597067b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee13c2ad-2b54-4abd-aa8e-d4668d453582",
            "compositeImage": {
                "id": "2e4b846d-6451-4ef7-901b-bfebcbc01053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a69e16d-3a76-427b-98c8-fcf9e597067b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07852313-f623-464f-9d0e-30ec8918fcba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a69e16d-3a76-427b-98c8-fcf9e597067b",
                    "LayerId": "c72f2e7d-4388-4d66-abfc-64683c18e835"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c72f2e7d-4388-4d66-abfc-64683c18e835",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee13c2ad-2b54-4abd-aa8e-d4668d453582",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}