{
    "id": "047cb358-1403-4722-99a8-2bfa18ea7149",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_spike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3c2386c-0497-4635-b9c5-470a46317251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047cb358-1403-4722-99a8-2bfa18ea7149",
            "compositeImage": {
                "id": "15270a04-3183-4cc7-8737-6e30d2ff5e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3c2386c-0497-4635-b9c5-470a46317251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5275fa-9bdb-41da-b361-d0cc9bab5838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3c2386c-0497-4635-b9c5-470a46317251",
                    "LayerId": "373aa2e1-4450-44f1-bb1b-d7ae2cda607c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "373aa2e1-4450-44f1-bb1b-d7ae2cda607c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "047cb358-1403-4722-99a8-2bfa18ea7149",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}