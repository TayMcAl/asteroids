{
    "id": "e1ab8780-5bb8-40ac-9e8c-6e532635bcf1",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path0",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "e5f7fe8c-3b22-462f-a23d-e728b4e319de",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 736,
            "speed": 100
        },
        {
            "id": "916dc638-7165-44b6-8870-61cd11c31abb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 640,
            "speed": 100
        },
        {
            "id": "b6c8d00a-19a1-4bf2-84a3-4d17feb9dc01",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 448,
            "speed": 100
        },
        {
            "id": "496c4d8c-f96e-4366-86d0-f99ad95df0dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 768,
            "speed": 100
        },
        {
            "id": "4d908cb7-7531-4dad-b81e-d597ffae143d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 448,
            "speed": 100
        },
        {
            "id": "eb7b8c4d-553b-44d6-94d1-c8ed7d967054",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 768,
            "speed": 100
        },
        {
            "id": "8030d043-c31f-429b-8e6d-4e608efb33aa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 448,
            "speed": 100
        },
        {
            "id": "cd1a404d-7ace-4963-b2f2-a78c34796a17",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 768,
            "speed": 100
        },
        {
            "id": "62c94497-7d6f-469b-a2ed-71d6888d7f5e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 448,
            "speed": 100
        },
        {
            "id": "59b730a2-93c4-4cdd-854d-faf64b913b05",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 768,
            "speed": 100
        },
        {
            "id": "8267fc1f-c3ee-4d88-b6ed-bd318ea910a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 512,
            "speed": 100
        },
        {
            "id": "080bb717-79e5-4daf-9f06-23eed28a3bb8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 768,
            "speed": 100
        },
        {
            "id": "76135ce5-a3ad-4185-8236-f5f950000b20",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 448,
            "speed": 100
        },
        {
            "id": "64a8cc2c-a8e0-4104-924f-80f284c0d16d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 768,
            "speed": 100
        },
        {
            "id": "5f662026-96a5-4507-ac42-56c208b2d946",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 448,
            "speed": 100
        },
        {
            "id": "7837931b-d8af-47f7-b137-5f7cfefeb212",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 768,
            "speed": 100
        },
        {
            "id": "06638133-9710-4b39-83aa-7d68ca966142",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 448,
            "speed": 100
        },
        {
            "id": "8073fed5-73f6-42ac-841f-8584f6dcc8fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 768,
            "speed": 100
        },
        {
            "id": "87607506-3306-417a-b718-81495d0f1bf0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 736,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}