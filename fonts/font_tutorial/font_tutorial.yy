{
    "id": "3ac3201a-4546-422a-9016-f6b07fc9b53a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_tutorial",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Franklin Gothic Book",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a2053d00-7315-4f4b-bd2f-52c660f1c3de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 94,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9d23bf10-d2de-4d67-b847-25b88fce7ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bfad6c0b-d8d8-4328-b515-a40d07e77a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4663ef50-139f-4569-a03c-fd7d648b2d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 22
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0de059b8-8fa4-45f8-8bd8-61b392fc8829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 22
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "02f7ee49-6024-423e-806d-5a34a94041dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ec903468-c7c2-4178-a02a-c25a0ba91584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7eb33d68-fc36-483e-86a3-0ab33cbaf8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 105,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "595661bc-104e-4786-a171-63ab74d92f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "247e8ef2-3733-45da-9d64-9d8647e6905b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 70,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "51b1a534-2d33-4583-9f49-28ec1717465b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 206,
                "y": 22
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "736ddc7e-d307-454f-b7bc-1a3f08ff036b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 22
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0757f0d7-a405-4f29-855f-72ba7402b6b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6e7d99c9-c9b5-4d2c-a638-918365b1f2d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0d3288f9-0c04-495f-ab61-655bda54ae87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 122,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "71a6e8a1-0739-4b17-b116-0f55ffb4ceec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 92,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "34ad9af9-3c52-49fa-bfb8-db085e41828d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ded22c7c-fa0a-49d2-a76c-4dc99b33d601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e93d200e-9349-4210-8fc1-06db8971b577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fd739b54-fe5b-498b-af57-5f020681bb91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c51f31fe-80c8-43d4-b31e-4426f52546ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9344ac70-5a63-4807-969e-b54756764518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "350af974-135e-4138-872e-829aff12d664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 22
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3053330f-c4fd-47ba-a37c-b2f3517e03ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "de1eb575-ff9e-4484-8539-3f8ed1a1ddea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 22
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ff73b281-ed06-4f5d-aae8-6e634baafda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 22
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a284a1b5-84e2-4b06-ba3c-998b0100f87c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e9a38c22-2b8f-4a55-9b64-0e29d4900e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 130,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a2a780ab-df97-4344-8f71-21f14d347342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 239,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c8d116e5-c1b3-4453-a862-10232393f4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7e254f77-bbeb-4554-9a07-58b2e00a9aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 42
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "15e48162-d81f-45b1-b6aa-187a0ad916e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 42
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9db0e643-f40d-4c90-93c7-3e959919e179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c3d19953-7792-4fae-a231-6fdc7639a9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b1b0d3c6-de13-4279-a2a0-33ea1db55ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bb79f99a-2f68-46be-9171-7b618bad70be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "46055996-f142-4315-a163-b8f14ee19a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d6c6f372-d9eb-4300-a573-575f1e603c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d3772c69-30b1-4139-b39f-b54c07260676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 167,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7fb775f9-a754-4eef-b72a-92121aea23d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "88115d81-a0ae-49b6-9b23-9269c01eeaea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e6dec993-b9f9-470f-8869-65b4050cd143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3ca68898-137f-4343-9749-6d4b89cb39ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "30540c86-4aa4-4046-bb93-164831a998b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6710df8e-3bc9-4b08-9c59-81858ff8ae9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 131,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6c57c04b-6ac5-43a7-8b2c-78e78a56c6f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c3b2d550-04a0-449d-a9c6-8f9a7f4dc814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "91910d23-c3d3-4178-b837-152b13d3017d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8ed36d74-6ebe-4bbe-bfe4-55ab5f823e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bf4359b1-366d-40cd-a435-35302cf13fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "802cc9d4-051a-4c55-a635-bde040f33083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b8e21de4-e686-46dd-a3d0-5e092d1db0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cdd46eaa-9f29-44ca-a80a-be48ac1e11e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 156,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "849416a7-e41c-4688-849d-745261ce3de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0dc53758-a9fd-439f-99d9-4e29353535e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cfba3399-3f02-4acf-883e-4071c832c45d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3bab2540-9ba0-4fc4-b903-187b5631e3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2edde8f3-8116-4dd4-94d2-7635024b241d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f25b7394-43d6-4b5d-975d-33662e4582a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "907ba92f-25fe-472e-99e1-bb2bbb622f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e668e32c-6593-4059-99af-2783650a4479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 9,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "04220176-8972-4c50-8fea-64f5cd962bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 88,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "366647e9-c249-4ffc-a483-a00d5e067039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "227a0ff8-e472-40db-a183-2cf90c941156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "041b1ad2-80a1-4ba8-a8c3-db922d9005db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 10,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e91bbad9-4ce0-4bac-9ac2-eaf1a54f8522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 216,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d12738cb-76be-4ff7-af78-2e2dbd811ebe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 212,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "00e47709-0442-4da4-82ce-665402148a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 221,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "776c7891-6d4d-4f84-a20d-6e4e7d5ad91a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 226,
                "y": 22
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "07bb55df-3252-454f-9547-289a5bc9e0ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 236,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7dcdc5b6-2332-4869-b0bd-ec45a9b227b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "05a3b1a8-87eb-474a-9440-b9c4d6add495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "71343090-abdf-4b9e-8286-3d3f8bb85e10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 140,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6f0a084b-2c4d-4c63-8b91-44196b44f2ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3f02beb9-21b0-4cd5-a3b5-1208185de8fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "418bad93-e546-450f-b4c9-66f0f457de9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 203,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0c651f5d-b4d5-4f60-b415-cb6a474e90b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 118,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fb334007-56bd-45ec-bf47-b53f531447f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7788065b-7461-4883-b6e0-cf29fb78e439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f3f571e6-d150-4e2c-a9a4-edbcd4839f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7dbaa2b8-21a2-4f8e-ae35-1d584573f42b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 176,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "25472d2c-48dd-4196-b62b-f783a5388b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 176,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3a5eb428-7ffb-4929-93d9-2f7ed0f8d34d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 31,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d4c397cb-f790-40d1-a614-1a09bf672da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f1c03fd2-4e45-41b8-bb23-bf577ab44a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "149cea71-b7c1-435b-87d1-b1bf32a20dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 185,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f8f1cb38-3d4c-4177-b197-4a19f6d4d4c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1f797da5-a501-48e0-aa02-bf35924ba54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8fd925f7-d603-4c63-b07f-ff074df4950c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "47f2230e-76c9-4c2b-aaf8-9c1525dac284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 149,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2b942ba2-55f7-4f13-a8be-8b37b3e1feb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 194,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "99b57d99-e098-4ecc-a103-1e5ce8502943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "59ca1089-432c-40a9-8a79-69af120c2b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bc0c0b15-fd62-470b-ad72-ff0fd0906e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "32b0d8e3-8d67-4665-a5ef-e04518bbc0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d104c22b-c719-48a9-ad7f-fb8aa83a94bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 55
        },
        {
            "id": "3d0e4875-dab9-4189-9388-d0f8d8b243ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 55
        },
        {
            "id": "7a70778e-2724-4d0c-a229-87fd414def4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 55
        },
        {
            "id": "13c049e0-5cfa-4063-acc1-1699298a9948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 49
        },
        {
            "id": "5181491d-feb9-4667-8dd8-eaf5542d35eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 55
        },
        {
            "id": "e88038d2-f5b5-4f16-9d85-c69fd74b44e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 49
        },
        {
            "id": "eea24d51-5483-49f0-8b9a-7fdc5a74efc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 46
        },
        {
            "id": "2646961f-cc91-4f8a-9e6a-64c7b5f76d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 49
        },
        {
            "id": "c41239cc-6585-4402-a9c3-a2053c0214a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 52
        },
        {
            "id": "71f81a44-7468-4c00-84fa-8f385092c09b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 54
        },
        {
            "id": "8cbf4d57-4f0f-4ee3-a0df-8a7e98d66b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "a961bf6d-1872-4dba-b160-7bff2f7c4870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 55
        },
        {
            "id": "dee1a0d9-066b-43eb-bd0d-8a0fbb87dda3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "5138daea-c147-4ff3-804a-194944c808b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "570058a5-0b67-4263-a206-bc3855038057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "1ae7afba-52aa-47ae-a01e-d352fa2459aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "b03e9319-b9eb-46df-880b-b420a5de89e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "dc3a518d-d466-4597-8a08-b3934d56a2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "aa7592f3-bf13-4868-8a5c-86cfb5bfec6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "80ca2c8e-dc6d-41f0-9bca-8c596251fd04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "c78eb63c-3a21-4abd-972a-c4a060f2b248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "4e645249-fc30-4a84-ba7c-2121a5eaa7bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "8ed4329d-9f50-4057-8581-13b0adc1eed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "324adbc5-57d1-4071-ac3e-59e538e67c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "6b115574-7862-4642-878e-64076c755cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "935846a5-707f-4823-a01b-ccabfcda7e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "c326ac0b-ff17-4df1-b0d1-ade50153dcd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "afa33016-d27a-4973-89fa-702c394b5c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "e7265589-52be-44b0-a000-fb48be15b369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "2db4f11b-70bd-4cd1-8b4b-11013356eb8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "d315e15a-964d-42f6-8d0b-80e9a406dcd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "d434553d-d4b0-4449-ae0c-df16b2300faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "0a783235-21d8-4b35-988b-0856d0aad930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "10ccb245-eed0-48d5-b904-ef7a4ae461a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "ec9420c5-0ee0-4923-8e72-84161e02db6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f2ad2733-3556-4cb5-9fe6-ca229b532d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "c07fda96-6c37-4508-85d1-a9c87628d3ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "f4141c01-236d-422d-9f79-85056bbb085e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "4a6a06cc-6fe9-4b40-8bfa-74a895f90ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "4915a687-42a1-4328-b560-2583da424154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "1fd520e4-6800-4836-b53e-164cbf66f850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "a57ced94-16f4-474e-9b80-9e781b083d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "68dd9d64-7bb9-4be7-bc3b-f00238bc4dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "3ea5d947-a47c-4e55-a0d1-54ddb6b85a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "ce5de59b-bee2-4bf8-a92d-cf6dae8dce54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "fe01f800-5cd5-48d2-9614-d9059b42b34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "a98dd40e-4da2-4893-be46-06bc8091a5a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "e5c880d4-1657-49f3-8a37-d66df92b00dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "c83184ff-e98a-4507-a9fe-ea0cb7336776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "b378745e-c513-4f72-85ea-31e0c5674348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "8e1e89fe-5097-42d7-81d9-3ce2ea7049ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "41a94e18-8108-4f60-998a-ef21e64ac80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "364f5308-52d3-4061-a2ae-d93191ff1f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "74d8aad1-c00c-49cc-9327-40ceba80e90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "c7483507-451f-4711-b642-f8de504c11f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "24829fc5-84b5-41c7-81b0-871e3d83dbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "39823fe2-7d3c-4926-80c4-e99028e94c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "e080a22d-a57c-454f-b2f7-85b182a1488b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "a43ac62f-5a22-4a86-8bbb-0d7fccfdce44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "2d7d955a-4642-4b4c-92bc-d8d764ce7829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}