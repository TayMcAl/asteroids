{
    "id": "b2b1a155-9fa5-43d3-85d9-0cb9b91b6bad",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_guy",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Freestyle Script",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "50641a60-6355-4f5a-ac3b-1a91c1a77c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 44,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 163,
                "y": 140
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8bd23fb6-c569-4986-a255-1be9e9f6079b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 44,
                "offset": -1,
                "shift": 10,
                "w": 15,
                "x": 218,
                "y": 94
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d7ca5775-d92a-44f7-a4dd-dfd17bdf97f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 44,
                "offset": 6,
                "shift": 9,
                "w": 10,
                "x": 103,
                "y": 140
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ffb3ce8d-86bc-4e51-85de-bbd165024755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 44,
                "offset": -1,
                "shift": 13,
                "w": 17,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0b63fd5d-7e8b-4067-8a27-7f413e66a807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 44,
                "offset": -3,
                "shift": 13,
                "w": 19,
                "x": 351,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "29cbb3f9-c82e-4aef-99e7-8d28e0f77074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 44,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 330,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "389e57a7-836a-495e-aa3b-ed44d3134948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 44,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f13c2013-c7f0-4a87-8b49-2a51f66d443c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 44,
                "offset": 6,
                "shift": 5,
                "w": 5,
                "x": 203,
                "y": 140
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dc56a51c-a959-4818-a64c-30310bd16403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 44,
                "offset": 1,
                "shift": 13,
                "w": 17,
                "x": 40,
                "y": 94
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "820c9e82-9bac-4735-a0d3-0a81da9611f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 44,
                "offset": -5,
                "shift": 13,
                "w": 17,
                "x": 490,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "57c3be82-d808-4065-99df-c51a13848520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 44,
                "offset": 6,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2d560886-6061-4d57-9784-41a4f2e72972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 44,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 435,
                "y": 94
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2af17408-2318-4605-9372-a19c41eedc18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 44,
                "offset": -4,
                "shift": 8,
                "w": 6,
                "x": 195,
                "y": 140
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f75e10b7-abfa-4cab-a214-74ee6df612a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 44,
                "offset": -2,
                "shift": 9,
                "w": 10,
                "x": 151,
                "y": 140
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "269672a4-f217-4b21-8d43-8a3d0283810b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 44,
                "offset": -2,
                "shift": 8,
                "w": 4,
                "x": 217,
                "y": 140
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2eff6567-1d1d-447e-a0da-b664bce8eca3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 44,
                "offset": -9,
                "shift": 12,
                "w": 29,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f01c4123-61d8-4c9d-bd35-f30c738a7435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 44,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 286,
                "y": 94
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e4bea666-6548-489e-882e-053705cda786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 44,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 140
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2676bb5b-e06c-481f-8f05-f211d3480324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 44,
                "offset": -3,
                "shift": 13,
                "w": 16,
                "x": 95,
                "y": 94
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ca26e5d8-0a3a-4854-8fed-7f03db2c4465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 44,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 333,
                "y": 94
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ad3c6e1c-8e8e-4c29-b95c-72643d86599a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 44,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 235,
                "y": 94
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e3fa63ec-a919-4802-92d9-cba32aef4f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 44,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 252,
                "y": 94
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ffcffaef-601f-495e-bca0-37dbfc6e60ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 44,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 348,
                "y": 94
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "afac2049-6897-4462-b2d6-54edf08a92ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 44,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 302,
                "y": 94
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f3b97dd0-d0b5-489e-a9f8-2453594faf1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 44,
                "offset": -2,
                "shift": 13,
                "w": 15,
                "x": 269,
                "y": 94
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "11d0bad5-e320-454e-aa7e-80f8400b7ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 44,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 421,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "66df3c18-5b5e-4dbe-8c31-7fb87bfa064e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 44,
                "offset": -2,
                "shift": 8,
                "w": 8,
                "x": 185,
                "y": 140
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6f8bc830-c4a4-4d9f-ae9c-ea35a00ccbfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 44,
                "offset": -4,
                "shift": 8,
                "w": 10,
                "x": 67,
                "y": 140
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6397e989-31ac-46c0-8dfd-4c4144626735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 432,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "50a3a1b1-b3ec-4ad6-8417-e4794816cffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 44,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 407,
                "y": 94
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5c1d2ecb-fe3b-4d2e-9833-53ff8bb67acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 372,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f9c3d361-9446-41f0-a8ab-88b80b0efe2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 44,
                "offset": 2,
                "shift": 14,
                "w": 16,
                "x": 149,
                "y": 94
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ab5ffc54-791d-4562-a1ff-568fd1e7a40e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 44,
                "offset": 3,
                "shift": 25,
                "w": 23,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "59add26c-69c1-47ec-8374-19e2f488cf58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 44,
                "offset": -3,
                "shift": 16,
                "w": 21,
                "x": 48,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "633d38df-151b-48f2-8e07-b4ed4bf6260d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 44,
                "offset": -2,
                "shift": 17,
                "w": 21,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e2c671d6-5f4d-4cd0-b9e3-00cd5b6d7d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 44,
                "offset": -1,
                "shift": 15,
                "w": 19,
                "x": 288,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b571229b-b809-45c8-b6fa-eaebaf71f765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 44,
                "offset": -1,
                "shift": 17,
                "w": 20,
                "x": 137,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "691330e6-b67d-4d03-8fac-e2683491c292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 44,
                "offset": -1,
                "shift": 15,
                "w": 23,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "72690a45-accf-4284-a566-0910417c9d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 44,
                "offset": -1,
                "shift": 15,
                "w": 23,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "86c3d736-cda9-45bb-b81b-52086605d816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 44,
                "offset": 0,
                "shift": 15,
                "w": 19,
                "x": 246,
                "y": 48
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "87ce0462-c1ac-4185-aa1a-98a1d79e3c3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 44,
                "offset": -1,
                "shift": 17,
                "w": 23,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8f95f20c-0d88-4848-8f6d-7aaaa18f412d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 44,
                "offset": -1,
                "shift": 8,
                "w": 15,
                "x": 201,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6e2e574a-e306-488c-be9d-b43af720a06a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 44,
                "offset": -1,
                "shift": 16,
                "w": 24,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "98397bbc-d994-4224-8f8f-a00c0adc93ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 44,
                "offset": -1,
                "shift": 17,
                "w": 23,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "aab835c9-f790-464d-879c-d576c52558d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 44,
                "offset": -2,
                "shift": 11,
                "w": 16,
                "x": 77,
                "y": 94
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b827bdde-bd26-4129-bc9d-e491ac061ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 44,
                "offset": -3,
                "shift": 21,
                "w": 28,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "92afd6b6-08a1-4cb7-b527-7bb70a6926b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 44,
                "offset": -2,
                "shift": 18,
                "w": 25,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e5494cea-fbce-4004-a036-5644ae61d48c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 44,
                "offset": -1,
                "shift": 17,
                "w": 20,
                "x": 203,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a5a5b3c7-9e8c-4b26-9141-03ce76da472a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 20,
                "x": 181,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "698d0479-fb7c-4f3a-8cf3-707e4572cade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 44,
                "offset": -1,
                "shift": 16,
                "w": 20,
                "x": 71,
                "y": 48
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0a17e6e1-d3f5-4268-b94e-030307fa7034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 44,
                "offset": -1,
                "shift": 16,
                "w": 21,
                "x": 25,
                "y": 48
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4b65fd57-7833-47cb-bda9-01e2ff934b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 44,
                "offset": -1,
                "shift": 15,
                "w": 20,
                "x": 93,
                "y": 48
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c3c60dda-e5b9-4680-944e-586ebcbf183e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 44,
                "offset": 2,
                "shift": 17,
                "w": 24,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f5352528-2a9b-4610-a80f-eff31c66ea5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 44,
                "offset": 0,
                "shift": 19,
                "w": 24,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8102319e-98d5-48b0-b256-c4fb96ace1ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 44,
                "offset": 2,
                "shift": 15,
                "w": 19,
                "x": 225,
                "y": 48
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bab9660e-9892-4680-87c2-67356e433015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 44,
                "offset": 0,
                "shift": 21,
                "w": 26,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e961fca8-d9ab-4b60-8bcb-fc40fbbb7154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 44,
                "offset": -3,
                "shift": 16,
                "w": 25,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8ed7b0f7-770c-4c98-bfd7-48c22100e2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 44,
                "offset": 2,
                "shift": 15,
                "w": 20,
                "x": 115,
                "y": 48
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "37ea3d72-f777-469f-a6c2-737bea2b2f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 44,
                "offset": -2,
                "shift": 14,
                "w": 23,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "66fb7595-c82a-4889-b021-f8028bc17f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 22,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1c1cebaf-ba46-46fa-9b55-ae95c31fa6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 44,
                "offset": 5,
                "shift": 12,
                "w": 9,
                "x": 174,
                "y": 140
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ef6e2d5f-754e-4130-a7f6-a15205a0b280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 44,
                "offset": -8,
                "shift": 16,
                "w": 23,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f6a3dc81-31ff-4190-8db8-5d35aa066f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 44,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 94
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7bd53668-ffaa-41d5-b729-e2e55393a256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 267,
                "y": 48
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "596a67e0-1336-4e42-9528-cff9493f268d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 44,
                "offset": 10,
                "shift": 19,
                "w": 5,
                "x": 210,
                "y": 140
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ebf83f84-cb72-4afe-a4e0-b4bd11bb1722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 140
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7c8f1b80-cd91-412c-858d-176d903c8397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 13,
                "x": 378,
                "y": 94
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "92365c92-546e-4d7f-8a31-6f452e74af94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 10,
                "x": 139,
                "y": 140
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d622ba84-9db6-48d5-ae18-8703d2bb613b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 16,
                "x": 59,
                "y": 94
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "29578566-5326-4843-998f-d3659f2af238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 15,
                "y": 140
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9aa1d06a-2731-4d6b-8250-07ccfa501e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 44,
                "offset": -4,
                "shift": 9,
                "w": 17,
                "x": 471,
                "y": 48
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ed18daec-7aa0-4c31-a5f0-61ca51418aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 44,
                "offset": -7,
                "shift": 11,
                "w": 18,
                "x": 392,
                "y": 48
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8cd4f6bd-670a-4216-919d-2003d4039498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 44,
                "offset": -2,
                "shift": 12,
                "w": 13,
                "x": 363,
                "y": 94
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a10de461-0a28-40d8-abf0-996c18100de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 44,
                "offset": -3,
                "shift": 5,
                "w": 11,
                "x": 41,
                "y": 140
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bc356e0c-0105-47be-865d-89ba1f7ced29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 44,
                "offset": -10,
                "shift": 8,
                "w": 20,
                "x": 159,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "47da46a0-9c7c-49fc-aaac-005d61b0bd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 44,
                "offset": -2,
                "shift": 13,
                "w": 17,
                "x": 452,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "380ba1ec-dee8-4ef0-bcd9-94e6c1d3f819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 44,
                "offset": -2,
                "shift": 8,
                "w": 16,
                "x": 131,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4404691a-43c1-438c-b099-ee51c6172dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 44,
                "offset": -2,
                "shift": 13,
                "w": 15,
                "x": 167,
                "y": 94
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4fde42d7-d730-45b4-8d07-456dd54274a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 28,
                "y": 140
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "92969eee-4ae8-4371-bff0-fb28be9b0bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 44,
                "offset": -2,
                "shift": 9,
                "w": 10,
                "x": 91,
                "y": 140
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "122b9ae8-20b3-4535-864d-650f3ae73607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 44,
                "offset": -7,
                "shift": 10,
                "w": 16,
                "x": 113,
                "y": 94
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e36d7dc2-7aa7-481e-81c2-909c966d73b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 44,
                "offset": -2,
                "shift": 9,
                "w": 10,
                "x": 115,
                "y": 140
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "21e40b6e-4598-4922-ad8a-433c4d89c13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 44,
                "offset": -2,
                "shift": 11,
                "w": 13,
                "x": 318,
                "y": 94
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "de79be47-1ea6-46e3-9ab2-51edbd44cf05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 44,
                "offset": -2,
                "shift": 9,
                "w": 12,
                "x": 393,
                "y": 94
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3b6f1030-59e3-4104-99f0-09231ca6f4bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 44,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 491,
                "y": 94
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d2fb8d56-c503-47e5-b85b-4a965a88e6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 10,
                "x": 79,
                "y": 140
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1e60975d-41ce-48fa-a40b-bf4e60c9748f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 44,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 449,
                "y": 94
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "29633623-0c27-4711-893e-36a12d4c5bbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 44,
                "offset": -2,
                "shift": 14,
                "w": 15,
                "x": 184,
                "y": 94
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f94476ad-4dc0-41ac-90e7-94522159de43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 44,
                "offset": -3,
                "shift": 9,
                "w": 12,
                "x": 477,
                "y": 94
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1233bcb2-e228-4cbc-ad55-0f5ca40f2fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 44,
                "offset": -6,
                "shift": 12,
                "w": 18,
                "x": 412,
                "y": 48
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ac3b57b2-ea9e-4f3f-8b05-3e2e0ec23369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 44,
                "offset": -3,
                "shift": 8,
                "w": 12,
                "x": 463,
                "y": 94
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bc3029cd-c124-4885-a4e0-d9b94f21260a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 22,
                "x": 446,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c255ae9d-c6cd-4d3b-a887-7d397e210dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 44,
                "offset": 9,
                "shift": 19,
                "w": 3,
                "x": 223,
                "y": 140
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e5ebefce-6836-4fb9-b5cf-a588480bef50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 44,
                "offset": -8,
                "shift": 16,
                "w": 22,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f6bfc338-b672-4489-b81c-141baa29ac9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 44,
                "offset": 4,
                "shift": 25,
                "w": 19,
                "x": 309,
                "y": 48
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}